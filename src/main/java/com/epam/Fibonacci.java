package com.epam;

import java.util.Scanner;


public class Fibonacci {

  static boolean isFibonacci(int arrayMember) {

    int p = 5 * arrayMember * arrayMember + 4;
    int sqrtOfP = (int) Math.sqrt(p);
    int r = 5 * arrayMember * arrayMember - 4;
    int sqrtOfR = (int) Math.sqrt(r);

    return ((p == sqrtOfP * sqrtOfP) || (r == sqrtOfR * sqrtOfR));
  }


  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);

    System.out.println("Введіть межі інтервалу: ");
    System.out.print("x1 = ");
    int start = scan.nextInt();
    System.out.print("x2 = ");
    int end = scan.nextInt();
    int length = end - start + 1;

    int[] Array = new int[length];
    Array[0] = start;

    int count = start + 1;
    for (int j = 1; j < length; j++) {

      Array[j] = count;
      count = count + 1;

    }

    for (int j = 0; j < length; j++) {
      System.out.print(Array[j] + " ");
    }

    int oddTotal = 0;
    int evenTotal = 0;
    System.out.println();
    System.out.println("Непарні числа від х1 до х2: ");
    for (int j = 0; j < length; j++) {
      if (Array[j] % 2 != 0) {
        System.out.print(Array[j] + " ");
        oddTotal += Array[j];
      }

    }
    System.out.println();
    System.out.println("Парні числа від х2 до х1: ");
    for (int j = length - 1; j >= 0; j--) {
      if (Array[j] % 2 == 0) {
        System.out.print(Array[j] + " ");
        evenTotal += Array[j];
      }

    }
    System.out.println();
    System.out.println("Сума парних чисел у проміжку: " + evenTotal);
    System.out.println("Сума непарних чисел у проміжку: " + oddTotal);

    //ArrayList<int> fibonacciList = new ArrayList<int>();

    // int[] fArray = new int [length];
    // fArray[0] = 1;
    // fArray[1] = 1;

    // for(int i=2; i < end; i++){
    //    fArray[i] = fArray[i-1] + fArray[i-2];
    // }

    System.out.println("Кількість: ");
    int N = scan.nextInt();
    int[] fArray = new int[N];

    int countF = 0;
    System.out.println("fibo:");
    for (int y = 0; y < length; y++) {
      if (isFibonacci(Array[y]) == true) {
        if (countF < N) {
          //System.out.println(Array[y] + " ");
          fArray[countF] = Array[y];
          countF++;
        }
      }

    }

    if (countF < N) {
      System.out.println("В інтервалі існує тільки " + countF + "чисел Фібоначчі");
    }

    //System.out.println("fibo:");

    if (fArray[N - 1] % 2 == 0) {
      System.out.println(fArray[N - 1] + "  - найбільше парне число");
      System.out.println(fArray[N - 2] + "  - найбільше непарне число");

    } else {
      System.out.println(fArray[N - 1] + "  - найбільше непарне число");
      System.out.println(fArray[N - 2] + "  - найбільше парне число");
    }

    System.out.println("fibo:");
    for (int j = 0; j < N; j++) {
      System.out.print(fArray[j] + " ");
    }

    //for (int j = 0; j<length; j++) {
    //System.out.println(Array [j] + " ");
    //}

  }


}





